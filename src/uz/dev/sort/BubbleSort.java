package uz.dev.sort;

import java.util.Arrays;

public class BubbleSort {

    int[] array = {99,6,2,8,2,4,5,55,1};
    int count = 1;

    void sort122(int[] array){
        for (int i = 0; i < array.length; i++) {
            for (int j = i+1; j < array.length; j++) {
                if(array[i] > array[j]) {
                    int buf = array[i];
                    array[i] = array[j];
                    array[j] = buf;
                }
            }
        }
    }

     void sort(int[] array) {
         for (int i = 0; i < array.length; i++) {
             for (int j = 0; j < array.length-1-i; j++) {
                 if(array[j] > array[j+1])
                     swap(j);
             }
         }
    }

    //ULUG'BEK AKA VERSION
//    void sort() {
//        for (int i = 0; i < array.length; i++) {
//            for (int j = i+1; j < array.length; j++) {
//                if(array[i] < array[j])
//                    swap(i, j);
//            }
//        }
//    }


    //    void sortWithError() {
//        for (int i = 0; i <array.length ; i++) {
//            for (int j = i; j < array.length-1; j++) {
//                if(array[j] < array[j+1])
//                    swap(j);
//            }
//        }
//    }



    void swap( int j){
        //System.out.println("in= "+Arrays.toString(array));
        int buf = array[j];
        array[j] = array[j+1];
        array[j+1] = buf;
        System.out.println("out= " + count++ +Arrays.toString(array));
     }
}

package uz.dev.sort;

import java.util.Arrays;

public class SelectionSort {
    int[] array = {99,6,2,8,2,4,5,55,1};
    int count = 1;

        void sort() {
        for (int i = 0; i < array.length; i++) {
            int minIndex = i;
            for (int j = i+1; j < array.length; j++) {
                if(array[j] > array[minIndex]) {
                    minIndex = j;
                }
            }
            swap(i, minIndex);
        }
    }

    void swap( int i, int j){
        //System.out.println("in= "+Arrays.toString(array));
        int buf = array[i];
        array[i] = array[j];
        array[j] = buf;
        System.out.println("out= " + count++ + Arrays.toString(array));
    }
}
